console.log('Hello World');

/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

let displayUserDetails = function(){
	let userFullName = prompt('What is your name?');
	let userAge = prompt('How old are you?');
	let userAddress = prompt('Where do you live?');

	alert("Thank you for entering your personal details!");
	console.log('Hello, ' + userFullName);
	console.log('You are ' + userAge + ' years old');
	console.log('You live in ' + userAddress);
}

displayUserDetails();

/*  2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function displayFavoriteMusicalArtists(){
	console.log('1. Nicki Minaj');
	console.log('2. Lizzo');
	console.log('3. TWICE');
	console.log('4. Post Malone');
	console.log('5. The Weeknd');
}

displayFavoriteMusicalArtists();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/

function displayFavoriteMovies(){
	console.log('1. Avengers: Endgame');
	console.log('Rotten Tomatoes Rating: 97%');
	console.log('2. Everything Everywhere All at Once');
	console.log('Rotten Tomatoes Rating: 95%');
	console.log('3. Inception');
	console.log('Rotten Tomatoes Rating: 87%');
	console.log('4. Fall');
	console.log('Rotten Tomatoes Rating: 76%');
	console.log('5.Army of the dead');
	console.log('Rotten Tomatoes Rating: 67%');
}

displayFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();
let printFriends = function(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};

printFriends();
// console.log(friend1);
// console.log(friend2);